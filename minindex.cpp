#include<iostream>
#include"minindex.h"

int Minindex(int *mass, int size)
{
    int min = mass[0];
    int minindex = 0;
        for (int i = 0; i < size; i++)
        {
            if (mass[i] < min)
            {
                min = mass[i];
                minindex = i;
            }
        }
        return minindex+1;
}
