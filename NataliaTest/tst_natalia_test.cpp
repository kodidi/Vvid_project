#include <QString>
#include <QtTest>

#include "C:\Proga\vvidkkp\minindex.cpp"
class Natalia_Test : public QObject
{
    Q_OBJECT

private slots:
    void test_minindex();
};

void Natalia_Test::test_minindex()
{
    int *mass = new int[4];
    mass[0] = 10;
    mass[1] = 6;
    mass[2] = 3;
    mass[3] = 7;
    QCOMPARE(Minindex(mass, 4),3);
}


QTEST_APPLESS_MAIN(Natalia_Test)

#include "tst_natalia_test.moc"
