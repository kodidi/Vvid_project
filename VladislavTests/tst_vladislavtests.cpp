#include <QString>
#include <QtTest>
#include "C:\Proga\vvidkkp\vladpart.cpp"

class VladislavTests : public QObject
{
    Q_OBJECT

private slots:
    void test_tmh();
    void test_comp();
    void test_del();
};

void VladislavTests::test_comp()
{
    int *mass = new int[4];
    mass[0] = 10;
    mass[1] = 6;
    mass[2] = 3;
    mass[3] = 7;
    int *mass2 = new int[4];
    mass2[0] = 10;
    mass2[1] = 6;
    mass2[2] = 3;
    mass2[3] = 7;
    QCOMPARE(compairTwoMass(mass,4,mass2),true);
    mass2[3] = 6;
    QCOMPARE(compairTwoMass(mass,4,mass2),false);

}

void VladislavTests::test_tmh()
{
    int *mass = new int[4];
    mass[0] = 10;
    mass[1] = 6;
    mass[2] = 3;
    mass[3] = 7;
    int *mass2 = threeMostHigh(mass, 4);
    QCOMPARE(mass2[0],10);
    QCOMPARE(mass2[1],7);
    QCOMPARE(mass2[2],6);
    int *mass3 = new int[2];
    mass3[0] = 7;
    mass3[1] = 10;
    QCOMPARE(threeMostHigh(mass3, 2),mass3);


}

void VladislavTests::test_del()
{
    int *mass = new int[4];
    mass[0] = 10;
    mass[1] = 6;
    mass[2] = 3;
    mass[3] = 7;
    mass = DeletingOneElem(mass, 4, 3);
    QCOMPARE(mass[0],10);
    QCOMPARE(mass[1],6);
    QCOMPARE(mass[2],7);
}
QTEST_APPLESS_MAIN(VladislavTests)

#include "tst_vladislavtests.moc"
