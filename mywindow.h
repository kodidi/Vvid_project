#ifndef MYWINDOW_H
#define MYWINDOW_H

#include <QtWidgets>
#include <QVBoxLayout>
#include <QString>


class mywindow: public QWidget
{
    Q_OBJECT
public:
    QStandardItemModel* model;
    QVBoxLayout* mainLay;
    QPushButton* addMassKey;
    QPushButton* addMassRand;
    QPushButton* Sort;
    mywindow(QWidget* pwgt=0);
    QPushButton* Maxelem;
    QPushButton* twomass;
    QPushButton* maxlenght;
    QPushButton* index;
    QPushButton* minIndex;
    QTableView* pTableView;
    QTextEdit* printMass;
    int *mass;
    int *mass2;
    int *mass3;
    int *mass1;
    int size, i =0;

public slots:
    void addingMassKey();
    void addingMassRand();
    void findMaxelem();
    void costyl();
    void lenghts();
    void searchMinIndex();
    void compTwoMass();


};

#endif // MYWINDOW_H
