#include "vladpart.h"
#include <ctime>

int *threeMostHigh(int *&mass, int size)
{
    if (size <= 3)
    {
        return mass;
    }
    else
    {
        int *mass2;
        int *mass3;
        mass2 = new int[size];
        for (int j = 0; j < size; j ++ )
        {
            mass2[j] = mass[j];
        }
        for (int i = 0; i < size; i++)
        {
            for (int j = i + 1; j < size; j++ )
            {
                if (mass2[j]>mass2[i])
                {
                    int k = mass2[i];
                    mass2[i] = mass2[j];
                    mass2[j] = k;
                }
            }
        }
        mass3 = new int[3];
        for (int i = 0; i < 3; i++)
        {
            mass3[i] = mass2[i];
        }
        return mass3;
    }
}

bool compairTwoMass(int *&mass, int size, int *&mass2)
{
    if (size > 0)
    {
        bool same = true;
        for (int i = 0; i < size; i++)
        {
            if(mass2[i] != mass[i])
            {
                same = false;
            }
        }
        if (same == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

int *DeletingOneElem(int *&mass, int size, int Elem)
{
    int *mass2 = new int[size - 1];
    Elem = Elem - 1;
    for (int i = 0; i < Elem; i++)
    {
        mass2[i] = mass[i];
    }
    for (int i = Elem + 1; i < size; i++)
    {
        mass2[i-1] = mass[i];
    }
    delete mass;
    return mass2;
}
