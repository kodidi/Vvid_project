#include<QtWidgets>
#include <QVBoxLayout>
#include"mywindow.h"
#include <QString>
#include <QTableView>
#include "vladpart.h"
#include"minindex.h"
#include"function.h"

mywindow::mywindow(QWidget *pwgt):QWidget(pwgt)
{
    addMassKey=new QPushButton("заполнить массив с клавиатуры");
    addMassRand=new QPushButton("заполнить массив случайно");
    Sort=new QPushButton("Отсортировать");
    Sort->setEnabled(false);
    Maxelem=new QPushButton("три наибольших елемента");
    Maxelem->setEnabled(false);
    twomass=new QPushButton("сравнение двух массивов");
    twomass->setEnabled(false);
    maxlenght=new QPushButton("самая длинная последовательность");
    maxlenght->setEnabled(false);
    index=new QPushButton("поиск индекса елемента");
    index->setEnabled(false);
    minIndex=new QPushButton("индекс минимального елемента");
    minIndex->setEnabled(false);
    printMass = new QTextEdit;

    connect(addMassKey, SIGNAL(clicked(bool)), this, SLOT(addingMassKey()));
    connect(addMassRand, SIGNAL(clicked(bool)), this, SLOT(addingMassRand()));
    connect(Maxelem, SIGNAL(clicked(bool)), this, SLOT(findMaxelem()));
    connect(Sort, SIGNAL(clicked(bool)), this, SLOT(costyl()));
    connect(twomass, SIGNAL(clicked(bool)), this, SLOT(compTwoMass()));
    connect(maxlenght, SIGNAL(clicked(bool)), this, SLOT(lenghts()));
 //   connect(index, SIGNAL(clicked(bool)), this, SLOT(sortingByPower()));
    connect(minIndex, SIGNAL(clicked(bool)), this, SLOT(searchMinIndex()));

    mainLay=new QVBoxLayout;
    mainLay->addWidget(printMass);
    mainLay->addWidget(addMassKey);
    mainLay->addWidget(addMassRand);
    mainLay->addWidget(minIndex);
    mainLay->addWidget(Sort);
    mainLay->addWidget(twomass);
    mainLay->addWidget(Maxelem);
    mainLay->addWidget(maxlenght);
    mainLay->addWidget(index);

    setLayout(mainLay);
    resize(750,500);
}

void mywindow::addingMassKey()
{
    size = QInputDialog::getInt(0, "размер", "введите размер массива", 1, 1);
    mass = new int[size];
    QString supportstr = QString::number(size);
    printMass->append("массив размером " + supportstr);
    for (int i = 0; i < size; i++)
    {
        supportstr = QString::number(i+1);
        QString str = "Введите элмент номер " + supportstr;
        mass[i] = QInputDialog::getInt(0, "массив", str, 0);
        QString supportstr2 = QString::number(mass[i]);
        printMass->append(supportstr2 + " ");
    }
    mass1 = new int[size];
    for(int i = 0; i < size; i ++)
        mass1[i]=mass[i];
    Sort->setEnabled(true);
    Maxelem->setEnabled(true);
    twomass->setEnabled(true);
    maxlenght->setEnabled(true);
    index->setEnabled(true);
    minIndex->setEnabled(true);
}

void mywindow::addingMassRand()
{
    size = QInputDialog::getInt(0, "размер", "введите размер массива", 1, 1);
    QString supportstr = QString::number(size);
    printMass->append("массив размером " + supportstr);
    mass = new int[size];
    srand( time(0));
    for (int i = 0; i < size; i++)
    {
        mass[i] = rand() % 100;
        QString supportstr2 = QString::number(mass[i]);
        printMass->append(supportstr2 + " ");
    }
    mass1 = new int[size];
    for(int i = 0; i < size; i ++)
        mass1[i]=mass[i];
    Sort->setEnabled(true);
    Maxelem->setEnabled(true);
    twomass->setEnabled(true);
    maxlenght->setEnabled(true);
    index->setEnabled(true);
    minIndex->setEnabled(true);
}

void mywindow::costyl(){

    int l = 0, r = size;
    Sort1(mass1, mass, l, r);
    for (int i = 0; i < size; i++)
    {
        QString supportstr2 = QString::number(mass[i]);
        printMass->append(supportstr2 + " ");
    }
}

void mywindow::lenghts(){
    int* ot = NULL;
    ot = lenght(size, mass);
    printMass->append("саммая длиная последовательность:");
    for (int i = ot[0]; i < (ot[0] + ot[1]); i++)
    {
        QString supportstr2 = QString::number(mass[i]);
        printMass->append(supportstr2 + " ");
    }

}

void mywindow::findMaxelem()
{
    mass3 = threeMostHigh(mass, size);
    if (size < 3)
    {
        printMass->append("наибольшие элементы:");
        for (int i = 0; i < size; i++)
        {
            QString supportstr2 = QString::number(mass3[i]);
            printMass->append(supportstr2);
        }
    }
    else
    {
        printMass->append("3 наибольших элемента: ");
        for (int i = 0; i < 3; i++)
        {
            QString supportstr2 = QString::number(mass3[i]);
            printMass->append(supportstr2);
        }
    }
}

void mywindow::searchMinIndex()
{
    QString supportstr = QString::number(Minindex(mass,size));
    printMass->append("Индекс минимального элемента: " + supportstr);
}

void mywindow::compTwoMass()
{
    mass2 = new int[size];
    QString str2;
    QString supportstr;
    printMass->append("Второй массив: ");
    for (int i = 0; i < size; i++)
    {
        supportstr = QString::number(i+1);
        QString str = "Введите элмент номер " + supportstr;
        mass2[i] = QInputDialog::getInt(0, "заполнение второго массива", str, 0);
        QString supportstr2 = QString::number(mass2[i]);
        str2 += supportstr2 + " ";
    }
    printMass->append(str2);
    bool comp = compairTwoMass(mass, size, mass2);
    if (comp == true)
        printMass->append("массивы одинаковы");
    else
            printMass->append("массивы не одинаковы");
}
